import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by samuel on 14.10.15.
 */
public class TaskRepository {
    private List<Task> tasks = new ArrayList<>();

    public List<Task> getAllTasks() {
        return new ArrayList<>(tasks);
    }

    public Optional<Task> getTask(String id) {
        // TODO replace with lambda
        return tasks.stream().filter(t -> t.getId().equals(id)).findAny();
    }

    public void createTask(Task task) {
        tasks.add(task);
    }

    public void updateTask(Task task) {
        // TODO replace with lambda
        tasks.removeIf(t -> t.getId().equals(task.getId()));
        tasks.add(task);
    }

    public void deleteTask(Task task) {
        tasks.remove(task);
    }
}