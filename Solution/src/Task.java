import java.util.UUID;

/**
 * Created by samuel on 14.10.15.
 */
public class Task {
    private String id;
    private String description;
    private Person assinged;
    private TaskState state;

    public Task(String description) {
        this.id = UUID.randomUUID().toString();
        this.description = description;
        this.state = TaskState.TODO;
    }

    public Task(String description, Person assinged) {
        this(description);
        this.assinged = assinged;
    }

    public Task(String description, TaskState state) {
        this(description);
        this.state = state;
    }

    public Task(String description, Person assinged, TaskState state) {
        this(description, assinged);
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Person getAssinged() {
        return assinged;
    }

    public void setAssinged(Person assinged) {
        this.assinged = assinged;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (id != null ? !id.equals(task.id) : task.id != null) return false;
        if (description != null ? !description.equals(task.description) : task.description != null) return false;
        if (assinged != null ? !assinged.equals(task.assinged) : task.assinged != null) return false;
        return state == task.state;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (assinged != null ? assinged.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' +
                ", assinged=" + assinged +
                ", state=" + state +
                '}';
    }
}