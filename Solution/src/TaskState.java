/**
 * Created by samuel on 14.10.15.
 */
public enum TaskState {
    TODO, IN_PROGRESS, DONE
}