import java.util.List;

/**
 * Created by samuel on 14.10.15.
 */
public class Main {
    public static void main(String[] args) {
        new Main();
    }

    private TaskRepository taskRepository = new TaskRepository();

    private Person klaus = new Person("Klaus", "Müller");
    private Person anna = new Person("Anna", "Müller");

    public Main() {
        init();
        run();
    }

    private void init() {
        taskRepository.createTask(new Task("Task 0"));
        taskRepository.createTask(new Task("Task 1", klaus));
        taskRepository.createTask(new Task("Task 2", TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 3", TaskState.DONE));
        taskRepository.createTask(new Task("Task 4", anna, TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 5", klaus, TaskState.DONE));
        taskRepository.createTask(new Task("Task 6", TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 7", TaskState.DONE));
        taskRepository.createTask(new Task("Task 8", klaus, TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 9", anna));
    }

    private void run() {
        List<Task> tasks = taskRepository.getAllTasks();
        System.out.println("All tasks:");
        tasks.forEach(System.out::println);

        // TODO get any task and assign it to anna
        tasks.stream().findAny().ifPresent(t -> {
            t.setAssinged(anna);
            taskRepository.updateTask(t);
        });

        System.out.println("Anna's tasks:");
        // TODO print all Tasks by Anna
        tasks.stream().filter(t -> anna.equals(t.getAssinged())).forEach(System.out::println);

        // TODO delete all DONE tasks
        tasks.stream().filter(t -> t.getState() == TaskState.DONE).forEach(taskRepository::deleteTask);

        tasks = taskRepository.getAllTasks();

        // TODO get all tasks by klaus and update the tasks with state IN_PROGRESS to DONE
        tasks.stream().filter(t -> klaus.equals(t.getAssinged())).filter(t -> t.getState() == TaskState.IN_PROGRESS).forEach(t -> {
            t.setState(TaskState.DONE);
            taskRepository.updateTask(t);
        });

        tasks = taskRepository.getAllTasks();

        // TODO update all tasks with state TODO to IN_PROGRESS
        tasks.stream().filter(t -> t.getState() == TaskState.TODO).forEach(t -> {
            t.setState(TaskState.IN_PROGRESS);
            taskRepository.updateTask(t);
        });

        tasks = taskRepository.getAllTasks();

        System.out.println("Klaus tasks:");
        // TODO assign all unassigned tasks to klaus and print them
        tasks.stream().filter(t -> t.getAssinged() == null).forEach(t -> {
            t.setAssinged(klaus);
            taskRepository.updateTask(t);
            System.out.println(t);
        });

        tasks = taskRepository.getAllTasks();

        // TODO check if any task is DONE
        boolean anyIsDone = tasks.stream().anyMatch(t -> t.getState() == TaskState.DONE);

        // TODO check if all tasks are assigned
        boolean allAssigned = tasks.stream().allMatch(t -> t.getAssinged() != null);

        System.out.println("Assigned persons:");
        // TODO print all assigned persons distinct
        tasks.stream().map(Task::getAssinged).distinct().forEach(System.out::println);

        tasks = taskRepository.getAllTasks();

        // TODO sort all tasks by description
        tasks.sort((t0, t1) -> t0.getDescription().compareToIgnoreCase(t1.getDescription()));

        System.out.println("All updated tasks:");
        tasks.forEach(System.out::println);
    }
}