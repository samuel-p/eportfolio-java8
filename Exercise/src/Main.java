import java.util.List;

/**
 * Created by samuel on 14.10.15.
 */
public class Main {
    public static void main(String[] args) {
        new Main();
    }

    private TaskRepository taskRepository = new TaskRepository();

    private Person klaus = new Person("Klaus", "Müller");
    private Person anna = new Person("Anna", "Müller");

    public Main() {
        init();
        run();
    }

    private void init() {
        taskRepository.createTask(new Task("Task 0"));
        taskRepository.createTask(new Task("Task 1", klaus));
        taskRepository.createTask(new Task("Task 2", TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 3", TaskState.DONE));
        taskRepository.createTask(new Task("Task 4", anna, TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 5", klaus, TaskState.DONE));
        taskRepository.createTask(new Task("Task 6", TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 7", TaskState.DONE));
        taskRepository.createTask(new Task("Task 8", klaus, TaskState.IN_PROGRESS));
        taskRepository.createTask(new Task("Task 9", anna));
    }

    private void run() {
        List<Task> tasks = taskRepository.getAllTasks();
        System.out.println("All tasks:");
        tasks.forEach(System.out::println);

        // TODO get any task and assign it to anna

        System.out.println("Anna's tasks:");
        // TODO print all Tasks by Anna

        // TODO delete all DONE tasks

        tasks = taskRepository.getAllTasks();

        // TODO get all tasks by klaus and update the tasks with state IN_PROGRESS to DONE

        tasks = taskRepository.getAllTasks();

        // TODO update all tasks with state TODO to IN_PROGRESS

        tasks = taskRepository.getAllTasks();

        System.out.println("Klaus tasks:");
        // TODO assign all unassigned tasks to klaus and print them

        tasks = taskRepository.getAllTasks();

        // TODO check if any task is DONE

        // TODO check if all tasks are assigned

        System.out.println("Assigned persons:");
        // TODO print all assigned persons distinct

        tasks = taskRepository.getAllTasks();

        // TODO sort all tasks by description

        System.out.println("All updated tasks:");
        tasks.forEach(System.out::println);
    }
}