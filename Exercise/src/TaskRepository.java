import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * Created by samuel on 14.10.15.
 */
public class TaskRepository {
    private List<Task> tasks = new ArrayList<>();

    public List<Task> getAllTasks() {
        return new ArrayList<>(tasks);
    }

    public Optional<Task> getTask(String id) {
        // TODO replace with lambda
        for (Task task : tasks) {
            if (task.getId().equals(id)) {
                return of(task);
            }
        }
        return empty();
    }

    public void createTask(Task task) {
        tasks.add(task);
    }

    public void updateTask(Task task) {
        // TODO replace with lambda
        for (Task t : new ArrayList<>(tasks)) {
            if (t.getId().equals(task.getId())) {
                tasks.remove(t);
            }
        }
        tasks.add(task);
    }

    public void deleteTask(Task task) {
        tasks.remove(task);
    }
}