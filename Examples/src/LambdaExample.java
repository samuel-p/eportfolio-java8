public class LambdaExample {
    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.run(() -> System.out.println("Hello World!"));
    }

    private static class Runner {
        void run(Executable executable) {
            System.out.println("Run executable:");
            executable.execute();
        }
    }

    @FunctionalInterface
    private interface Executable {
        void execute();
    }
}