import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class StreamExample {
    public static void main(String[] args) {
        List<Person> persons = new ArrayList<>();
        initPersons(persons);

        List<Person> adults = persons.stream().filter(p -> p.getAge() >= 18).collect(toList());
        System.out.println("Adults:");
        adults.forEach(System.out::println);

        int adultSum = adults.stream().mapToInt(Person::getAge).reduce((left, right) -> left + right).orElse(0);
        System.out.println("Adult age sum: " + adultSum);

        List<String> childrenNames = persons.stream().filter(p -> p.getAge() < 18).map(Person::getName).collect(toList());
        System.out.println("Children names:");
        childrenNames.forEach(System.out::println);

        boolean namesContainA = childrenNames.stream().anyMatch(name -> name.contains("a"));
        System.out.println("Children name contains 'a': " + namesContainA);
    }

    private static void initPersons(List<Person> persons) {
        persons.add(new Person("Hans", 14));
        persons.add(new Person("Anna", 17));
        persons.add(new Person("Rudolf", 19));
        persons.add(new Person("Klaus", 8));
        persons.add(new Person("Sarah", 20));
        persons.add(new Person("Katrin", 15));
        persons.add(new Person("Dieter", 18));
        persons.add(new Person("Laura", 21));
        persons.add(new Person("Jonas", 23));
        persons.add(new Person("Daniel", 12));
    }
}