/**
 * Created by samuel on 22.05.16.
 */
public class InterfaceExample {
    public static void main(String[] args) {
        Something something = new Something();
        something.sayHello();
        something.sayHi();
    }

    private static class Something implements Anything {
        @Override
        public void sayHello() {
            System.out.println("Hello!");
        }
    }

    private interface Anything {
        void sayHello();

        default void sayHi() {
            System.out.println("Hi!");
        }
    }
}