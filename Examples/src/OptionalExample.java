import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public class OptionalExample {
    public static void main(String[] args) {
        printName(of(new Person("Klaus", 8)));
        printName(empty());
    }

    private static void printName(Optional<Person> person) {
        String name = person.map(Person::getName).orElse("Default Name");
        System.out.println(name);
    }
}




